\documentclass[a4paper,10pt, twoside]{article}
\usepackage[utf8x]{inputenc}
\usepackage{graphicx}
\usepackage[brazil]{babel}
\usepackage{comment}
\usepackage{subcaption}
\usepackage{hyperref}

\graphicspath{ {./images/} }

\usepackage{geometry}
 \geometry{
 a4paper,
 total={170mm,257mm},
 left=20mm,
 top=20mm,
 }

%opening
\title{Holistically-Nested Edge Detection  \\
    \large Authors: Saining Xie and Zhuowen Tu
}
\author{Felipe Augusto Lima Reis \\
    \small Programa de Pós Graduação em Informática - PUC Minas \\
    {\tt\small falreis@sga.pucminas.br}
}

\date{}
\begin{document}

\maketitle

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\section{Introdução}

O artigo \textit{Holistically-Nested Edge Detection} \cite{HED:2015} procura abordar o problema de deteção de fronteiras e objetos em imagens naturais. O problema possui grande importância na área de visão computacional, com métodos bastante conhecidos na literatura. O problema, no entanto, ainda continua em aberto, uma vez que os algoritmos existentes ainda não conseguem identificar precisamente os segmentos.

%Os algoritmos clássicos em geral são não supervisionados e podem ser baseados em grafos \cite{SLIC:2012}, como métodos EGB \cite{Felzenszwalb:2004} e Lattices \cite{Lattices:2008} ou baseados em gradiente ascendente \cite{SLIC:2012}, como as abordagens Watersheds \cite{Watersheds:1991} e SLIC \cite{SLIC:2012}. Algumas abordagens mais recentes, como o método HED \cite{HED:2015}, proposto no artigo avaliado, utilizam métodos supervisionados baseados em redes neurais artificiais para segmentação e detecção de bordas.

O método desenvolvido pelos autores busca automatizar o processo de aprendizagem de características hierárquicas para detecção de bordas. Os autores utilizam o termo \textit{``holistically-nested''} com intuito de produzir um sistema de detecção de imagens de fim-a-fim, onde as saídas são refinadas pelos mapas de fronteiras produzidos pelas saídas laterais \cite{HED:2015}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{HED - Holistically-Nested Edge Detection}

O algoritmo realiza classificação imagem a imagem, seguindo o método \textit{Fully Convolutional Neural Networks} \cite{FCN:2014} e o método de aprendizagem multi-escala, de acordo com as redes supervisionadas profundas (\textit{Deeply-supervised nets}) \cite{deeply_super:2015}.

A rede neural desenvolvida pelos autores, chamada \textit{Holistically-nested networks} é uma variante do treinamento de múltiplas redes e pode produzir predições em múltiplas escalas \cite{HED:2015}. A rede possui múltiplas saídas laterais, conforme Figura \ref{fig:neural_net}, em uma ``rede neural profunda de fluxo único'' \cite{HED:2015}. A ilustração das saídas laterais da rede HED está disponível na Figura \ref{fig:side_output}.

\begin{figure}
\begin{subfigure}{0.5\textwidth}
  \centering
  \includegraphics[width=0.9\textwidth]{neural_net.png}
  \caption{Arquitetura HED. }
  \label{fig:neural_net}
\end{subfigure}%
\begin{subfigure}{0.5\textwidth}
  \centering
  \includegraphics[width=0.9\linewidth]{side_output_layers.png}
  \caption{Ilustração da saída lateral da rede HED}
  \label{fig:side_output}
\end{subfigure}
\caption{Arquitetura da rede HED e representação do resultado das saídas laterais \textit{Reprodução de \cite{HED:2015}.}}
\label{fig:hed_network}
\end{figure}

Para criação da rede neural HED foram utilizadas técnicas de \textit{transfer learning}. A rede foi baseada na arquitetura da VGGNet \cite{VGGNET:2014}. A rede foi modificada e foram adicionadas saídas laterais na última camada convolucional de cada estágio. Além disso, foram removidas as camadas completamente conectadas e a última camada de \textit{pooling}, para redução de processamento e custo computacional \cite{HED:2015}. Os pesos iniciais da rede foram obtidos a partir do treinamento da VGGNet.

Outra alternativa avaliada pelos autores foi a utilização da rede FCN (\textit{Fully Convolutional Network}) \cite{FCN:2014}. Foram utilizadas as redes FCN-8s e FCN-2s, com modificação da função de perda. Os resultados, entretanto, não foram tão bons quanto os obtidos usando a VGGNet.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Experimentos}

\subsection{Implementação}

A rede neural HED foi implementada usando o \textit{framework Caffe} e utilizando as implementações dos públicas das redes FCN \cite{FCN:2014} e DSN \cite{deeply_super:2015}. A inialização dos parâmetros foi feita para a rede VGG-16 \cite{VGGNET:2014}. Os parâmetros foram afinados até que fosse observado a convergência da rede.

Além do uso de \textit{transfer learning}, foram utilizadas as seguintes técnicas de aprendizado de máquina:

\begin{itemize}
 \setlength\itemsep{0em}
 \item \textit{Consensus Sampling}: o método de amostragem por consenso consiste em avaliar diferentes \textit{ground-truths} para verificar se um pixel pode ser classificado como correto ou não (utilizando ``votação'');
 \item \textit{Data augmentation}: as imagens da base foram rotacionadas em 16 diferentes angulos e, em cada uma delas, foi feito espelhamento da imagem, gerando ao todo 32 variações a partir da imagem original;
 \item \textit{Funções diferentes de pooling}: foi utilizada uma técnica para substituição de todos os valores das camadas de \textit{pooling} pelo valor do \textit{pooling} médio;
 %\item \textit{Interpolação bilinear em rede}: as previsões de saídas laterais foram implementadas em camadas deconvolucionais \textit{in-network}, com base em interpolação linear. A variação, entretanto, não produziu resultados significativos no modelo.
\end{itemize}

Os treinamento foi executado em uma GPU NVIDIA K40, com duração aproximada de 7h. Para uma imagem de 320x480, o algoritmo executa em 400ms (em GPU), produzindo o mapa final de bordas.

\subsection{Bases de Dados}

\begin{itemize}
 \setlength\itemsep{0em}
 \item \textit{BSDS500}: a base é composta de 500 imagens naturais segmentadas manualmente. A base é divida em 200 imagens de treinamento, 100 de validação e 200 de testes \cite{BSDS500:2011}.
 \item \textit{NYUDv2}: composta de 1449 imagens RGB-D (RGB + profundidade), a partir de vídeos de cenas em ambientes internos. A base divide-se em 381 imagens de treinamento, 414 de validação e 654 de teste \cite{NYDv2:2012}.
\end{itemize}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Resultados}

A curva de precisão e revocação obtida para a base de dados BSDS500 está ilustrada na Figura \ref{fig:bsds500}. Essa figura contém ainda um ponto correspondente ao limiar humano, com valor de \textit{F-score} 0.80, para comparação. Segundo \cite{Martin:2004}, a escolha do posicionamento das bordas é algo subjetivo entre pessoas e existe influência pessoal na definição do \textit{ground-truth} \cite{Marr:1980}, resultando em um valor médio de precisão humana para os \textit{ground-truths}.

Segundo os autores, com a expansão aleatória da base de treinamento do BSDS500 em 100 unidades, a partir da base de testes, melhorou consideravalmente o valor do $ODS$ (\textit{Optimal Data Set Scale}), correspondente ao valor médio para a base de dados. Os resultados original da base de dados subiu de $ODS=.782$ para $ODS=.797(\pm.003)$, chegando próximo ao limiar humano.

Os resultados obtidos para a base de dados NYUDv2 estão mostrados na Figura \ref{fig:nyudv2}. O algoritmo HED foi subdividido nesse teste, com adição de \textit{features} HHA (\textit{disparidade horizontal, altura do solo, angulo entre a superfície local e a direção inferida de gravidade}).

\begin{figure}
\begin{subfigure}{0.5\textwidth}
  \centering
  \includegraphics[width=1.\linewidth]{bsds500_curve.png}
  \caption{Curva de previsão e revocação na base BSDS500}
  \label{fig:bsds500}
\end{subfigure}%
\begin{subfigure}{0.5\textwidth}
  \centering
  \includegraphics[width=1.\linewidth]{nyudv2_curve.png}
  \caption{Curva de previsão e revocação na base NYUDv2}
  \label{fig:nyudv2}
\end{subfigure}
\caption{Curvas de precisão e revocação dos algoritmos avaliados nas bases de dados BSDS500 e NYUDv2. \textit{Reprodução de \cite{HED:2015}.}}
\label{fig:results_algs}
\end{figure}

\begin{comment}
\begin{figure}
\begin{subfigure}{0.5\textwidth}
  \centering
  \includegraphics[width=0.8\linewidth]{bsds500_results2.png}
  \caption{Resultados do algoritmo na base BSDS500}
  \label{fig:bsds500}
\end{subfigure}%
\begin{subfigure}{0.5\textwidth}
  \centering
  \includegraphics[width=0.8\linewidth]{nyudv2_results2.png}
  \caption{Resultados do algoritmo na base NYUDv2}
  \label{fig:nyudv2}
\end{subfigure}
\caption{Resultados do algoritmo nas bases de dados BSDS500 e NYUDv2. \textit{Adaptado de \cite{HED:2015}}. \\ \small \textit{ODS (Optimal Data Set Scale) = valor médio para a base de dados, OIS (Optimal Image Scale) = melhor valor para uma única imagem, AP (Average Precision) = precisão média \cite{Arbalaez:2009}}.  }
\label{fig:results_algs}
\end{figure}
\end{comment}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Conclusão}

O artigo, resumido nesse documento, desenvolveu uma rede neural convolucional para detecção de bordas com desempenho próximo ao limiar humano em imagens naturais da base de dados BSDS500, com desempenho de 0.4s em processamento usando GPU e 12s utilizando CPU. 

A construção do algoritmo utilizou diversas técnicas de aprendizado de máquinas como \textit{transfer learning}, \textit{consensus sampling} e \textit{data augmentation}. O código fonte dos autores está disponível online em \url{https://github.com/s9xie/hed}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\bibliography{supervised} %% the .bib file without extension
\bibliographystyle{plain}

\end{document}
