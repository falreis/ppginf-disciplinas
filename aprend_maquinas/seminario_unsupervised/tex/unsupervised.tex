\documentclass[a4paper,10pt, twoside]{article}
\usepackage[utf8x]{inputenc}
\usepackage{graphicx}
\usepackage[brazil]{babel}
\usepackage{comment}
\usepackage{subcaption}
\usepackage{hyperref}
\usepackage{float}

\graphicspath{ {./images/} }

\usepackage{geometry}
 \geometry{
 a4paper,
 total={170mm,257mm},
 left=20mm,
 top=20mm,
 }
 
 \renewcommand{\baselinestretch}{1.1} 
 \setlength{\parskip}{0.3em}

%opening
\title{Unsupervised Learning of Edges \\
    \large Authors: Yin Li, Manohar Paluri, James M. Rehg and Piotr Dollar
}
\author{Felipe Augusto Lima Reis \\
    \small Programa de Pós Graduação em Informática - PUC Minas \\
    {\tt\small falreis@sga.pucminas.br}
}

\date{}
\begin{document}

\maketitle

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\section{Introdução}

O artigo \textit{Unsupervised Learning of Edges} \cite{Li_CVPR:2016} procura abordar o problema de detecção de fronteiras e objetos em imagens. Os métodos de aprendizado na área de detecção de bordas e contornos são, em geral, supervisionados \cite{Li_CVPR:2016}. O artigo, no entanto, questiona se é necessária a utilização de supervisão para esse tipo de tarefa. Os autores empregam um método não supervisionado, onde o \textit{ground truth} é gerado com base na detecção de movimentos e geração de fronteiras, a partir de uma base de dados em vídeos.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Aprendendo Contornos a partir de Vídeos}

O artigo apresenta um processo de aprendizado não supervisionado para treinamento e detecção de contornos. O processo possui uma etapa de inicialização e em seguida é executado um laço de repetição com 4 etapas distintas, conforme a Figura \ref{fig:edge_detection_flow}, nos passos de 1-4. O laço correspondente aos passos 1-4 é executado até a convergência dos resultados para detecção das bordas.

\begin{figure}[H]
  \centering
  \includegraphics[width=0.9\textwidth]{semi_dense_matches.png}
  \caption{Processo de detecção de bordas e contornos em imagens. \textit{Reprodução de \cite{Li_CVPR:2016}}.}
  \label{fig:edge_detection_flow}
\end{figure}

O processo inicia-se com a seleção de um par de \textit{frames} consecutivos. Utilizando um gradiente simples, um conjunto de correspondências semi-densas é gerado a partir do par de \textit{frames}.

A partir das correspondências semi-densas e um mapa de contornos para o primeiro frame, é estimado o fluxo entre as imagens (Figura \ref{fig:edge_detection_flow}.1). Para essa tarefa é utilizado o fluxo óptico EpicFlow \cite{EPICFLOW:2015}, que calcula a distância geodésica entre os pixels e combina os vetores de movimentos, indicando a movimentação. Uma minimização posterior produz um mapa de fluxo de alta acurácia com preservação das bordas.

A detecção de bordas de movimentos corresponde ao passo seguinte do processo (Figura \ref{fig:edge_detection_flow}.2). Foi utilizado um detector de bordas treinado para estimativa de movimento. Ele foi aplicado a um mapa de fluxo codificado em cores, no espaço de cores HSV (Figura \ref{fig:motion_edge_detection}-b). A execução de um detector de bordas $\epsilon$ no mapa de fluxo colorido fornece um mecanismo para detecção de bordas (Figura \ref{fig:motion_edge_detection}-c).

\begin{figure}[H]
  \centering
  \includegraphics[width=0.9\textwidth]{motion_edge_detection_adapt.png}
  \caption{Processo de detecção de bordas de movimentos. \textit{Adaptado de \cite{Li_CVPR:2016}}.}
  \label{fig:motion_edge_detection}
\end{figure}

As fronteiras computadas a partir do fluxo podem ficar desalinhadas com a imagem correspondente. Para correção e alinhamento das bordas de movimento, foi aplicada uma heurística, onde as bordas de movimento foram alinhadas com as bordas dos superpixels SLIC \cite{SLIC:2012}. Bordas de movimento que puderam ser combinadas foram mantidas e as demais, descartadas. Esse refinamento está ilustrado na Figura \ref{fig:motion_edge_detection}-d.

Na etapa 3 (Figura \ref{fig:edge_detection_flow}.3) do processo é feito o treinamento do detector de bordas, com os algoritmos:

\vspace{-0.25cm}
\begin{itemize}
 \setlength{\itemsep}{0.1em}
 \setlength{\parskip}{0.1em}
 
 \item Structured Edges (SE) \cite{SE:2015}: O método baseado em \textit{random forest} extrai \textit{features} de baixo nível na imagem, como a cor para previsão de bordas.
 
 \item Holistic Edges (HE) \cite{HED:2015}: A rede neural é construída sobre uma versão adaptada da VGG-16 \cite{VGGNET:2014}, com saídas laterais que são combinadas, gerando uma previsão para classificação das imagens.
\end{itemize}

\vspace{-0.25cm}
\setlength{\parskip}{0.4em}

No último passo do processo, são aplicados os detectores de bordas em todos os \textit{frames} (Figura \ref{fig:edge_detection_flow}.4). Em seguida são realizadas iterações até a convergência (normalmente entre 3 e 4 iterações são suficientes).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Base de Dados para Treinamento}

Para geração dos mapas de fluxo e treinamento dos detectores de bordas, foram utilizados duas bases de dados: Video Segmentation Benchmark (VSB)\cite{VSB:2013} e YouTube Object Dataset \cite{YOUTUBE:2012}. As anotações foram desconsideradas. 

A coleção de vídeos das bases possui mais de 500 mil \textit{frames}. Devido ao alto número de amostras disponíveis, foi utilizada uma heurística para filtragem dos \textit{frames} mais adequados para estimativa dos movimentos. A heurística reduziu o número de \textit{frames} de treinamento para aproximadamente 50 mil.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Experimentos e Resultados}

\subsection{Detecção de Movimentos}

O primeiro experimento executado teve como objetivo avaliar o desempenho da detecção de movimentos. Foi usada base Video Segmentation Benchmark (VSB), que possui anotações de \textit{ground truth} a cada 20 frames. Os resultados, conforme Tabela \ref{sfig:motion_edge_table}, foram próximos ao melhor método, porém bem abaixo do limiar humano.

\subsection{Detecção de Bordas em Imagens}

O principal experimento realizado no artigo corresponde a detecção de bordas em imagens. Foram utilizados os \textit{ground truths} de movimentos das bordas disponíveis na base VSB para treinamento dos detectores SE e HE, que em seguida foram avaliados na base de dados BSDS500 \cite{BSDS500:2011}. O algoritmo HE teve duas versões: iniciando do modelo pré-treinado da ImageNet \cite{IMAGENET:2015} (HE) e também treinado desde o início (HE†). 

Os resultados dos testes estão disponíveis na Tabela \ref{sfig:bsds_table}. Percebemos que algoritmo HE, com dados pré-treinados da ImageNet tiveram os melhores resultados, com valor próximo ao limiar humano.

\subsection{Fluxo Óptico}

O fluxo óptico foi avaliado com as bases de dados Middlebury \cite{Baker:2011} e MPI Sintel \cite{Butler:2012}. Os resultados obtidos nos testes para variantes do método EpicFlow estão disponíveis na Figura \ref{sfig:optical_flow_table}. Os melhores resultados obtidos nos testes foram para a variação HE-BSDS do algoritmo, quando comparado às demais variações testadas.

\begin{figure}
\begin{subfigure}{0.35\textwidth}
  \centering
  \includegraphics[width=.9\linewidth]{motion_edge_results.png}
  \caption{Detecção de Movimentos}
  \label{sfig:motion_edge_table}
\end{subfigure}
\begin{subfigure}{0.3\textwidth}
  \centering
  \includegraphics[width=.8\linewidth]{precision_recall_table.png}
  \caption{Detecção de Bordas em Imagens}
  \label{sfig:bsds_table}
\end{subfigure}
\begin{subfigure}{0.35\textwidth}
  \centering
  \includegraphics[width=.9\linewidth]{epicflow_accuracy.png}
  \caption{Fluxo Óptico}
  \label{sfig:optical_flow_table}
\end{subfigure}
\caption{Resultado dos experimentos.\textit{Reprodução de \cite{Li_CVPR:2016}}}
\label{fig:results_tables}
\end{figure}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Conclusão}

O artigo resumido nesse documento desenvolveu um método para treinamento de detectores de bordas sem necessidade explícita de supervisão. Os resultados obtidos para o treinamento sem supervisão tiveram desempenho próximo ao de métodos supervisionados, indicando uma nova abordagem para o treinamento em base de dados sem anotações de \textit{ground-truth}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\bibliography{unsupervised} %% the .bib file without extension
\bibliographystyle{plain}

\end{document}
