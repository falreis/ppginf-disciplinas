###########################
###########################
# Author: Felipe A. L. Reis
# Date: 2018-06-02
# Email: falreis@gmail.com

#Benchmark to test AWS instances.
# - CPU benchmark
# - Memory benchmark
# - IO benchmark
# - Mutex benchmark

#Required softwares: 
# - Sysbench (tested in 1.0.11)

#Parameters:
# - param 1: name of test (aka. instance name)
# - param 2: number of execution (1, 2, 3, etc.) to avoid influence of shared CPUs
###########################
###########################


#create folder to benchmark
rm -r $1
mkdir $1
cd $1

echo "Test name: " $1

#############################################################
#################### Hardware specs #########################

sudo lshw >> "hardware.txt"

#############################################################


#############################################################
#################### CPU benchmark ##########################

for each in 1 2 4 8 16 32 64 128 256; do 
filename=$2"__cpu_"$each".txt";
DATE=`date '+%Y-%m-%d %H:%M:%S'`
echo "CPU: " $filename $DATE
#sysbench cpu --cpu-max-prime=200 --threads=$each run >> $filename;
sysbench cpu --cpu-max-prime=20000 --threads=$each --time=60 run >> $filename;
done

#############################################################


#############################################################
################### Memory benchmark ########################

for each in 1 2 4 8; do 
DATE=`date '+%Y-%m-%d %H:%M:%S'`
filename=$2"__mem_read"$each".txt";
echo "Memory read: " $filename $DATE
sysbench memory --memory-block-size=1K --memory-scope=global --memory-total-size=50G --memory-oper=read --threads=$each --time=240 run >> $filename;
done

for each in 1 2 4 8; do 
DATE=`date '+%Y-%m-%d %H:%M:%S'`
filename=$2"__mem_write"$each".txt";
echo "Memory write: " $filename $DATE
sysbench memory --memory-block-size=1K --memory-scope=global --memory-total-size=50G --memory-oper=write --threads=$each --time=240 run >> $filename;
done

#############################################################


#############################################################
###################### IO benchmark #########################

sysbench fileio --file-total-size=32G --file-num=64 prepare

#read only
for each in 1 2; do 
DATE=`date '+%Y-%m-%d %H:%M:%S'`
filename=$2"__io_read_"$each".txt";
echo "IO Read: " $filename $DATE
sysbench fileio --file-total-size=32G --file-test-mode=rndrd --time=180 --threads=$each --file-num=64 run >> $filename;
done

#mixed read and write
for each in 1 2; do 
DATE=`date '+%Y-%m-%d %H:%M:%S'`
filename=$2"__io_rw_"$each".txt";
echo "IO Read/Write: " $filename $DATE
sysbench fileio --file-total-size=32G --file-test-mode=rndrw --time=180 --file-fsync-all --threads=$each --file-num=64 run >> $filename;
done

sysbench fileio --file-total-size=32G cleanup

#############################################################


#############################################################
#################### Mutex benchmark ########################

for each in 1 2 4 8; do 
DATE=`date '+%Y-%m-%d %H:%M:%S'`
filename=$2"__mutex_"$each".txt";
echo "Mutex: " $filename $DATE
sysbench mutex --threads=$each --time=30 run >> $filename;
done

#############################################################


#go back to current folder
cd ..

#compress folder
zip -r $1.zip $1

