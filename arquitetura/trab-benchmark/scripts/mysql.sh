###########################
###########################
# Author: Felipe A. L. Reis
# Date: 2018-06-02
# Email: falreis@gmail.com

#Benchmark to test AWS instances.
# - Mysql benchmark

#Required softwares: 
# - Sysbench (tested in 1.0.11)
# - Mysql (tested in ...)

#Parameters:
# - param 1: name of test (aka. instance name)
# - param 2: number of execution (1, 2, 3, etc.) to avoid influence of shared CPUs
###########################
###########################


#create folder to benchmark
rm -r $1
mkdir $1
cd $1

echo "Test name: " $1

#############################################################
#################### Hardware specs #########################

sudo lshw >> "hardware.txt"

#############################################################
#################### MySQL benchmark ########################

filename=$2"__mysql.txt";

DATE=`date '+%Y-%m-%d %H:%M:%S'`
echo "Prepare MySQL" $DATE
sysbench --table-size=1000000 --db-driver=mysql --mysql-db=sysbench --mysql-user=sysbench --mysql-password=12345678 /usr/share/sysbench/oltp_read_write.lua prepare

for each in 1 2 4 8 16 32 64; do 
DATE=`date '+%Y-%m-%d %H:%M:%S'`
filename=$2"__mysql"$each".txt";
echo "Starting Mysql: " $DATE
sysbench --table-size=1000000 --db-driver=mysql --mysql-db=sysbench --mysql-user=sysbench --mysql-password=12345678 --time=120 --threads=$each /usr/share/sysbench/oltp_read_write.lua run >> $filename;
done

DATE=`date '+%Y-%m-%d %H:%M:%S'`
echo "Cleaning Mysql: " $DATE
sysbench --db-driver=mysql --mysql-db=sysbench --mysql-user=sysbench --mysql-password=12345678 /usr/share/sysbench/oltp_read_write.lua cleanup

#############################################################

#go back to current folder
cd ..

#compress folder
zip -r $1.zip $1

