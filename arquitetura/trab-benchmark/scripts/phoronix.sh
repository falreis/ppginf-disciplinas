###########################
###########################
# Author: Felipe A. L. Reis
# Date: 2018-05-31
# Email: falreis@gmail.com

#Benchmark to test AWS instances.
# - CPU benchmark
# - Memory benchmark
# - System benchmark

#Required softwares: 
# - Phoronix-Test-Suite (tested in 1.0.11)

###########################
###########################


#############################################################
#################### CPU benchmark ##########################

phoronix-test-suite benchmark pts/compress-gzip #60m #1
#phoronix-test-suite benchmark pts/pybench #20m #7
phoronix-test-suite benchmark pts/cachebench #30m #2
phoronix-test-suite benchmark pts/build-mplayer #60m #3
phoronix-test-suite benchmark pts/java-scimark2 #60m #4
#phoronix-test-suite benchmark pts/clomp #30m #6

#############################################################


#############################################################
################### Memory benchmark ########################

phoronix-test-suite benchmark pts/ramspeed #60m #5 
phoronix-test-suite benchmark pts/t-test1 #20m

#############################################################


#############################################################
#################### System benchmark ########################

#phoronix-test-suite benchmark pts/stress-ng #26m
#phoronix-test-suite benchmark pts/nginx #60m
phoronix-test-suite benchmark pts/phpbench #20m
phoronix-test-suite benchmark pts/apache #30m

#############################################################


