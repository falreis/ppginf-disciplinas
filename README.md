# About

This folder contains files and projects that I use to achieve my master's degree. 

Feel free to fork. Unfortunatelly I can't accept pull requests :(.

If you want to contact me, send me an email: [falreis@gmail.com](mailto:falreis@gmail.com). 

My personal personal website: [http://falreis.eng.br](http://falreis.eng.br).
