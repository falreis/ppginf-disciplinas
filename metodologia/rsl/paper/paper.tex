\iffalse\documentclass{article}\fi
\documentclass[10pt,conference]{IEEEtran}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage{tabularx}
\usepackage{etoolbox}
\usepackage{hyperref} %url
\usepackage{footnote}
\usepackage[]{algorithm2e}
\usepackage{xcolor}
\usepackage{comment}
\usepackage{footnote}
\usepackage{makecell}

%\renewcommand{\familydefault}{<Helvetica>}

%\usepackage{subfig}
%\captionsetup{belowskip=12pt,aboveskip=4pt}

\patchcmd{\thebibliography}{\section*{\refname}}{}{}{}

\graphicspath{ {./images/} }
\makesavenoteenv{tabular}
\makesavenoteenv{table}

%opening
\title{Redes Neurais Convolucionais para Detecção de Bordas - Uma Revisão Sistemática da Literatura}

\author{
    \IEEEauthorblockN{Felipe Augusto Lima Reis\IEEEauthorrefmark{1}}
    \IEEEauthorblockA{\IEEEauthorrefmark{1}PUC Minas - Pontifícia Universidade Católica de Minas Gerais \\falreis@sga.pucminas.br}
}

\begin{document} 

\maketitle

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{abstract}

Quais os métodos existentes na literatura para a detecção de bordas e contornos utilizando redes neurais convolucionais? Esses métodos atingiram os estado da arte nas bases de dados existentes? Outras técnicas possuem desempenho superior? Os métodos são capazes de gerar hierarquias das bordas? A partir da revisão sistemática da literatura, busca-se apresentar uma visão geral do uso de redes neurais convolucionais para a tarefa de detecção de bordas e contornos. Os resultados obtidos indicam que a técnica tem sido empregada de forma crescente nos últimos anos, atingindo o estado da arte em diferentes bases de dados. A RSL ainda detalha as principais técnicas desenvolvidas e a evolução dos valores de estado da arte para as bases BSDS500 e NYUDv2. Por fim, também analisa a existência de métodos capazes de gerar hierarquia multi-escala das bordas.

\end{abstract}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Introdução} 
\label{sec:introducao}

A identificação de partes ou objetos em imagens é uma tarefa simples para humanos, porém complexa para computadores. Abordagens tradicionais para segmentação e detecção de bordas (fronteira entre dois objetos ou duas regiões diferentes \cite{pedrini2008analise}) utilizam propriedades básicas das imagens em busca de encontrar descontinuidades que possam indicar a existência de objetos \cite{pedrini2008analise} \cite{gonzalez2002digital}. Técnicas mais recentes propõe métodos supervisionados usando redes neurais para detecção de bordas e segmentação, onde as características das imagens são aprendidas pelos algoritmos a partir de conjuntos de treinamento. Essas abordagens reduzem a necessidade da descoberta de padrões pelos próprios pesquisadores, deixando a cargo do software a tarefa de identificação de características mais adequadas à tarefa \cite{Segnet:2017:7803544}.

No processo de identificação de bordas é possível codificar a hierarquia de fronteiras em uma única imagem, chamada de mapa de contorno ultra-métrico \cite{Arbelaez:2006:1640630}. Nessa representação de bordas suaves, as cores mais escuras indicam segmentação com menor granularidade, enquanto cores mais claras possibilitam informações mais detalhadas. A representação possibilita cortes horizontais na estrutura, de modo a obter resultados com diferentes níveis de informação.

Devido ao sucesso do uso de redes neurais convolucionais para detecção de objetos e segmentação de imagens, e, recentemente, para detecção de bordas, faz-se necessário avaliar as técnicas mais empregadas e se os resultados obtidos superam os algoritmos tradicionais. A revisão sistemática de literatura fornece mecanismos para delineamento do panorama de uso do método.

O presente trabalho está dividido nas seguintes seções: a Seção \ref{sec:metodo} apresenta informações sobre Revisão
Sistemática da Literatura, metodologia de pesquisa utilizada nesse estudo; a Seção \ref{sec:resultados} exibe os artigos selecionados, com as notas nos critérios de qualidade; a Seção \ref{sec:discussao} discute e avalia os resultados encontrados, as técnicas, as bases de dados e as redes neurais utilizadas para confecção da arquiteturas propostas; e, por fim, a Seção \ref{sec:conclusoes} apresenta as considerações finais.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Revisão Sistemática da Literatura} 
\label{sec:metodo}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Questões de Busca} 
\label{ssec:quest_busca}

Esta pesquisa tem como objetivo realizar um levantamento de trabalhos que realizam detecção de bordas utilizando redes neurais convolucionais, a fim de obter um panorama da utilização da técnica. Para isso, foram propostas 3 Questões de Pesquisa [QP], a fim de obter o cenário atual de utilização da técnica, os resultados gerados e a possibilidade de geração de hierarquia das bordas:

\begin{itemize}
 \label{qp:questao1}
 \item \textit{\textbf{[QP1]} Qual o panorama na literatura de métodos de detecção de bordas e contornos que utilizam redes neurais convolucionais?}
 
 \label{qp:questao2}
 \item \textit{\textbf{[QP2]} Os métodos atingiram os estado da arte nas bases de dados avaliadas?}
 
 \label{qp:questao3}
 \item \textit{\textbf{[QP3]} Os métodos são capazes de gerar hierarquia multi-escala das bordas?}
\end{itemize}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Processo de Busca} 
\label{ssec:proc_busca}

A pesquisa aqui apresentada realizou o levantamento de artigos no período de 2013 a 2018. O período envolvido inicia-se poucos meses após a publicação do artigo \textit{ImageNet Classification with Deep Convolutional Neural Networks} \cite{Alexnet:2012:4824}, vencedor do concurso \textit{Large Scale Visual Recognition Challenge (LSVRC)}. Nele, o método descrito atingiu o estado da arte em acurácia para a classificação da base ImageNet \cite{HistoryAlexnet:2018}

A rede neural apresentada no artigo ficou posteriormente conhecida como AlexNet e constitui um marco no uso de redes neurais convolucionais profundas \cite{HistoryAlexnet:2018}. O artigo usou uma técnica até então pouco utilizada, que se mostrou revolucionária. Nos anos seguintes, todos os vencedores do concurso utilizaram versões melhoradas de redes neurais convolucionais para atingir o estado da arte, ultrapassando o limiar humano na tarefa \cite{HistoryAlexnet:2018}. A Figura \ref{fig:uso_redes_neurais} mostra a acurácia obtida na competição, o método vencedor e o número de camadas da rede neural utilizada.

\begin{figure}[!ht]
  \caption{Crescimento das redes neurais convolucionais após a rede Alexnet para a tarefa de classificação. Reprodução de \cite{AlexnetGPUUsage:2017}}
  \centering
  \includegraphics[width=1.\columnwidth]{gpu_usage_alexnet.png}
  \label{fig:uso_redes_neurais}
\end{figure}

A tarefa de detecção de bordas pode ser considerada próxima às tarefas de classificação e segmentação de imagens. Os métodos existentes são, inclusive, utilizados como auxiliares em métodos de classificação e segmentação. Com isso, justifica-se a escolha do período indicado.

Para desenvolvimento do trabalho foram escolhidas três bases de artigos, que abrangem a maioria dos trabalhos existentes na área de Visão Computacional e Reconhecimento de Padrões. As bases de artigos, método e período de busca estão disponíveis na Tabela \ref{table:bases_pesquisa}.

%\label{table:bases_pesquisa}
\input{tables/bases_pesquisa}

Foram buscados artigos escritos somente em língua inglesa, utilizando apenas o critério de \textit{metadata} (título, resumo e palavras chaves). Além disso, somente foram aceitos artigos completos em conferências, revistas e jornais. Artigos de acesso prévio, tutoriais, painéis, mini revisões, demonstrações, dentre outros, foram removidos da busca.

\footnotetext[1]{IEEE Xplore - \url{https://ieeexplore.ieee.org/search/searchresult.jsp}}
\footnotetext[2]{Science Direct- \url{https://www.sciencedirect.com/search/advanced}}
\footnotetext[3]{SCOPUS - \url{https://www.scopus.com/results/}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{\textit{String} de Busca} 
\label{ssec:string_busca}

Para que seja possível responder as questões elaboradas na Seção \ref{ssec:quest_busca} foi desenvolvida uma \textit{string} de busca, detalhada no pseudo-algoritmo \ref{alg:string_1}. Para possibilitar as consultas, a \textit{string} foi adequada ao padrão de cada uma das bases de artigos.

\begin{algorithm}
\footnotesize 
\begin{verbatim}
( "CNN" OR
  "convolutional neural network" OR 
  "convolutional neural networks"
) AND (
  "boundary detection" OR 
  "boundaries detection" OR 
  "edge detection" OR 
  "contour detection" OR 
  "border detection" OR 
  "saliency detection"
)
\end{verbatim}
\caption{\textit{String} de busca primária}
\label{alg:string_1}
\end{algorithm}

A \textit{string} desenvolvida fornece informações suficientes para elaborar um panorama do uso de redes neurais convolucionais para detecção de bordas, possibilitando resposta à questão \textit{QP1}. As respostas às questões \textit{QP2} e \textit{QP3}, relacionadas à eficiência dos métodos e a capacidade de geração de hierarquia de bordas, serão respondidas pela \textit{string} de busca com auxílio dos critérios de qualidade do artigo, definidos na Seção \ref{ssec:crit_qualidade}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Critérios de Inclusão e Exclusão}
\label{ssec:crit_inclusao_exclusao}

A metodologia utilizada para a produção da Revisão Sistemática de Literatura (RSL) foi composta por cinco fases:

\begin{itemize}
 \item Fase 1: remoção de artigos duplicados;
 \item Fase 2: remoção de erros na exportação;
 \item Fase 3: eliminação por título;
 \item Fase 4: eliminação por resumo;
 \item Fase 5: eliminação por leitura completa.
\end{itemize}

A remoção de artigos foi feita com auxílio de critérios de exclusão específicos em cada uma das fases, a fim de manter o maior número de trabalhos relevantes. Os critérios de exclusão definidos para cada uma das fases estão disponíveis na Tabela \ref{table:criterios_exclusao}.

%\label{table:criterios_exclusao}
\input{tables/criterios_exclusao}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Critérios de Qualidade}
\label{ssec:crit_qualidade}

Na Fase 5 foram aplicados critérios de qualidade, para classificar os resultados encontrados. Para cada critério foram dadas 3 possíveis notas: ``Atende totalmente'', ``Atende Parcialmente'' e ``Não atende''. Os valores respectivos de cada critérios foram 1, 0,5 e 0 pontos. O peso de cada critério foi definido de acordo com a Tabela \ref{table:criterios_qualidade}. Para que um artigo fosse aceito, foi necessário atingisse o percentual mínimo de 60\% (nota de corte $\geq$ 6).

%\label{table:criterios_qualidade}
\input{tables/criterios_qualidade}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Desvios do protocolo}
\label{ssec:desvios_protocolo}

Em uma avaliação inicial, foi utilizada a expressão \textit{"saliency detection"} na \textit{string} de busca devido a existência desse termo em alguns artigos relacionados ao tema. Porém, após a avaliação dos resultados, verificou-se que a expressão retornou diversos resultados que não estavam diretamente relacionados às questões de busca. Para solucionar esse problema, foi criado um filtro na fase de eliminação por título, a fim de remover exclusivamente os resultados que continham somente informações sobre detecção de saliências. 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Resultados} 
\label{sec:resultados}

Durante o estudo foram encontrados 851 artigos. Os artigos foram filtrados segundo os critérios de exclusão definidos na Seção \ref{ssec:crit_inclusao_exclusao}. Após a aplicação de todos os critérios de exclusão, foram selecionados apenas 15 artigos. O número de artigos removidos em cada Fase do processo de Revisão Sistemática de Literatura estão disponíveis na Tabela \ref{table:qtd_artigos}. 

%\label{table:qtd_artigos}
\input{tables/qtd_artigos}

A exibição visual dos resultados está disponível na Figura \ref{fig:fases_qtd_artigos}. Podemos ver uma considerável redução na quantidade de artigos (23,6\%) após aplicação dos filtros na Fase 1, resultando em 650 artigos. Nessa fase, percebemos que a quantidade de artigos removidos da base IEEE foi consideravelmente inferior às demais bases. Essa característica justifica-se apenas pelo \textit{script} utilizado, no qual a segunda ocorrência de um  artigo era eliminada. A não remoção de artigos do IEEE é justificada apenas pela ordenação inicial da lista de artigos.

O número de artigos removidos após a segunda fase pode ser considerado baixo (7,2\%, resultando em 603 artigos remanescentes). Foram desprezados nessa fase trabalhos que não são artigos científicos ou completos (91,4\%) e erros nos resultados retornados pelas bases (8,6\%).

Podemos verificar pela Figura \ref{fig:fases_qtd_artigos} que a maior redução na quantidade de artigos foi feita após a aplicação da Fase 3: Eliminação por Título. Nessa fase houve uma queda de 89,7\% na quantidade de artigos em relação a fase anterior. Devido a expressiva redução de artigos, é importante avaliar os critérios utilizados. Para isso, foram geradas a Tabela \ref{table:criterios_exclusao_fase3} e a Figura \ref{fig:fase_3_criterio}. 

%\label{table:criterios_exclusao_fase3}
\input{tables/criterios_exclusao_fase3}

\begin{figure}[!ht]
  \caption{Quantidade de artigos avaliados por fase da RSL}
  \centering
  \includegraphics[width=1.\columnwidth]{fases_qtd_artigos.png}\label{fig:fases_qtd_artigos}
\end{figure}

\begin{figure}[!ht]
  \caption{Motivo de exclusão de artigos, após Fase 3: Eliminação por Título}
  \centering
  \includegraphics[width=1.\columnwidth]{fase_2_remocao_por_criterio.png}
  \label{fig:fase_3_criterio}
\end{figure}

A análise dos resultados indica a existência de inúmeras pesquisas que contém aplicações dos métodos de detecção de bordas e contornos para solução de problemas práticos (16,8\%). Indica também a existência de diversos trabalhos relacionados apenas a segmentação, classificação e detecção de saliências (totalizando 22,7\%). Por fim, o critério majoritário de desclassificação de artigos, está relacionado ao assunto. Esse critério, responsável por 55,9\% das remoções, engloba exclusão de artigos cujos assuntos não estão diretamente ligados ao tema da RSL e artigos que não foram removidos por nenhum dos filtros anteriores. Os demais critérios de exclusão somaram 4,6\%.  

Analisando os trabalhos removidos na Fase 3, podemos verificar que mesmo uma busca somente utilizando os metadados retornou diversos artigos que não estão diretamente relacionados ao tema de pesquisa - detecção de bordas e contornos. Como diversos trabalhos não continham as expressões de busca no título, podemos inferir que a existência de palavras-chave genéricas ou a utilização de resumos no qual o assunto de detecção de bordas e contornos é citado como exemplo ou tomado como base para construção de uma linha de raciocínio.

Na Fase 4, foram eliminados artigos segundo o resumo. Nessa etapa, foram adotados dois motivos principais para eliminação de artigos: Assunto e Proposta. Após leitura dos resumos, foi verificado que 22 artigos não se encaixavam no tema pesquisado. Outros 13 artigos foram removidos devido a proposta, o que engloba o método utilizado, as técnicas adotadas ou o objetivo geral do artigo. 

Por fim, a última fase utilizada dessa RSL contém a leitura completa dos artigos. Para isso, durante e após a leitura dos artigos foram dadas notas aos critérios de qualidade definidos na Tabela \ref{table:criterios_qualidade}. A lista de todos os artigos avaliados, a nota em cada critério e a nota final estão disponíveis na Tabela \ref{table:artigos_selecionados}. Os artigos indicado em negrito tiveram nota de corte igual ou superior a 6 e foram selecionados por esta RSL.

%\label{table:artigos_selecionados}
\input{tables/artigos_selecionados}

É importante detalhar nessa última etapa os critérios para classificação dos artigos. No critério \textit{C1}, foram avaliadas a aplicabilidade dos métodos utilizados e a clareza das informações. Artigos que não eram claros sobre os métodos, objetivos e resultados tiveram nota 0. Artigos pouco claros ou que utilizaram o método de detecção de bordas apenas para auxiliar em outras tarefas receberam nota 0,5. Artigos claros e objetivos quanto aos métodos, objetivos e resultados tiveram nota igual a 1.

No critério \textit{C2}, foi avaliada a arquitetura da rede neural. Redes não convolucionais\footnote{Redes não convolucionais não fazem parte do escopo desta RSL. O único caso de rede não convolucional na Fase 5 foi eliminado por outros critérios.} e/ou com resultados muito baixos tiveram nota igual a 0. Redes neurais convolucionais com resultados intermediários tiveram nota igual a 0,5. Redes com bons resultados tiveram nota igual a 1. 

No critério \textit{C3}, avaliou a existência de comparação do método desenvolvido no artigo com outros métodos da literatura. Métodos que não apresentaram nenhum tipo de comparação com outros resultados da literatura tiveram nota 0. Métodos cuja comparação foi feita com até 3 métodos ou considerada insuficiente tiveram nota igual a 0,5. Demais artigos tiveram nota igual a 1.

O critério \textit{C4} avaliou a existência de informações de tempo de execução do algoritmo. Esse critério busca observar a velocidade de execução das redes e de métodos de pós processamento. O tempo de execução muitas vezes é ignorado pelos autores ou não é informado, quando o tempo de execução é muito alto. Artigos que não informaram tempo de execução tiveram nota igual a 0, enquanto artigos que apresentaram informações sobre o tempo de execução tiveram nota 1.

O critério \textit{C5} buscou observar o número de bases de dados públicas utilizadas no artigo. Essas bases de dados permitem a reprodução dos testes por outros autores. O número de bases de dados avaliadas também foi considerada na avaliação. Artigos que avaliaram mais de uma base de dados podem ser considerados mais completos, uma vez que um mesmo método foi submetido a mais de um problema ou uma coleção de dados. Também é mais difícil de ser escrito e indica, na maior parte das vezes, na visão desta RSL, um trabalho mais completo. Com isso, artigos que não avaliaram nenhuma base de dados tiveram nota 0. Artigos que avaliaram uma única base de dados tiveram nota igual a 0,5 enquanto artigos que utilizaram duas ou mais bases tiveram nota 1.

O critério \textit{C6} avaliou a existência de representação visual dos resultados. Como o trabalho avalia a detecção de bordas e contornos em imagens, é necessário mostrar os resultados por meio de figuras para os leitores, a fim de que seja possível visualizar a capacidade do método. Nesse critério, nenhum artigo recebeu nota 0. Um único artigo recebeu nota 0,5, pois somente exibiu uma imagem com o resultado do método, o que pode ser considerado insuficiente. Os demais artigos receberam nota 1.

Os critérios \textit{C7} e \textit{C8} avaliaram se o resultado atingiu o estado da arte em ao menos uma base de dados e se o método produziu hierarquia multiescala das bordas. Foi feita avaliação binária para ambos os critérios. Em relação ao critério \textit{C7} é importante ressaltar que não foi feita conferência sobre a data de publicação do artigo e o valor do estado da arte anterior, verificando se o método realmente atingiu o maior valor da literatura. A discussão sobre esse critério será mais bem detalhada na Seção \ref{ssec:bases_eficiencia_metodos}.

Por fim, o critério \textit{C9} avaliou a existência de código fonte publicamente disponível. Esse critério busca premiar artigos cujo código fonte está disponível para a comunidade, aumentando a transparência e permitindo a rápida reprodução dos experimentos. Artigos que não apresentavam código fonte tiveram nota igual a 0. Artigos que não apresentavam código fonte no texto, porém puderam ser facilmente encontrados na internet tiveram nota igual a 0,5. A mesma nota também foi dada a artigos cujo código estava incompleto, sem dados para treinamento ou sem os pesos finais da rede. Artigos cujo código fonte estavam completos e indicados no artigo tiveram nota igual a 1.

Para informação extra sobre as notas finais dos artigos selecionados na Fase 5, foi desenvolvida a Tabela \ref{table:estatisticas_artigos}, contendo informações estatísticas sobre os resultados finais.

%\label{table:estatisticas_artigos}
\input{tables/estatisticas_artigos}

Podemos ver pela Tabela \ref{table:estatisticas_artigos} que os artigos de 2018 possuem nota média mais alta e menor desvio padrão. Além disso, o ano de 2018 contém o maior número de artigos selecionados. Outras informações adicionais sobre o número de artigos está disponível na Seção \ref{ssec:panorama_cnn_bordas}.

%\subsection{Fatores de Qualidade} 
%\label{ssec:fatores_qualidade}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Discussão} 
\label{sec:discussao}

Esta seção busca responder e discutir as questões de pesquisa.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Panorama do uso de redes neurais convolucionais para detecção de bordas} 
\label{ssec:panorama_cnn_bordas}

Após a seleção e classificação dos artigos dessa RSL, é possível avaliar o uso de redes neurais convolucionais para detecção de bordas, a fim de traçar um panorama da técnica para solução do problema. Para isso, faz-se necessário avaliar o número de artigos encontrados por ano na pesquisa inicial e o número de artigos selecionados. Essas informações estão disponíveis na Figura \ref{fig:qtd_artigos_ano}.

\begin{figure}[!ht]
  \caption{Quantidade de artigos por ano}
  \centering
  \includegraphics[width=1.\columnwidth]{qtd_artigos_ano.png}
  \label{fig:qtd_artigos_ano}
\end{figure}

Podemos ver na Figura \ref{fig:qtd_artigos_ano} que tanto o número de artigos selecionados quanto o número de artigos encontrados aumentou ao longo dos anos pesquisados. Isto indica que a adoção de redes neurais convolucionais tem crescido para a solução de diferentes problemas na área de computação e também na tarefa de detecção de bordas e contornos. Considerando apenas os anos de 2013 e 2018, de início e fim do período escolhido, podemos ver que a quantidade de artigos em 2018 é 30,9 vezes superior a quantidade de artigos em 2013. Ainda é possível indicar que o uso de redes convolucionais para detecção de bordas teve início apenas em 2015 com os métodos DeepContour \cite{DeepContour:2015:7299024} e HED \cite{HED:2015:7410521}.

Conforme indicado na Seção \ref{ssec:proc_busca}, a utilização de redes neurais convolucionais para processamento de imagens iniciou-se com os bons resultados obtidos para tarefas de classificação. Espera-se, então, que as redes desenvolvidas para detecção de bordas utilizem a arquiteturas já existentes, com alterações para que seja possível atender ao novo objeto de pesquisa. Essa técnica, conhecida como \textit{transfer learning} ou transferência de conhecimento, é uma técnica para aprendizado no qual o conhecimento obtido na resolução de um problema é transferido para outro \cite{Pratt:1992:NIPS1992_641} \cite{Weiss:2016:JOURN_BIG_DATA}. Uma rede neural destinada a um problema é adaptada e aplicada a um problema diferente, porém relacionado. A lista de trabalhos e as redes neurais utilizadas como base está disponível na Tabela \ref{table:redes_bases}.

%\label{table:redes_bases}
\input{tables/redes_bases}

Conforme podemos verificar na Tabela \ref{table:redes_bases}, a maioria dos trabalhos selecionados (66,7\%) utiliza ao menos uma rede neural base para desenvolvimento do seu modelo. Dentre as redes usadas, destaca-se a rede VGG16 \cite{VGGNET:2014}, presente em 80\% dos trabalhos que utilizam redes neurais base. A rede ResNet \cite{RESNET:2016:7780459}, desenvolvida em 2016, é a segunda mais adotada. Apesar dos resultados superiores da rede ResNet na tarefa de classificação de imagens, alguns artigos, mesmo em 2018, continuam utilizando a rede VGG, devido ao menor custo de treinamento. Também foram citadas como arquiteturas base as redes DeepLab \cite{DeepLab:2014} e U-Net \cite{UNet:2015}.

Outro conhecimento importante que pode ser extraído das Tabela \ref{table:redes_bases} corresponde ao uso de saídas laterais nas redes neurais. Iniciado em 2015 na área de detecção de bordas pela rede HED \cite{HED:2015:7410521}, o uso desse artifício possibilitou aumento do desempenho nos últimos anos, com diversas propostas para combinação dos resultados parciais (laterais) em um único resultado final. No ano de 2018, todas as redes neurais para detecção de bordas utilizaram essa técnica.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Bases de dados e eficiência dos métodos} 
\label{ssec:bases_eficiencia_metodos}

A Questão de Pesquisa \textit{QP2} busca identificar os métodos que atingiram o estado da arte nas bases de dados para detecção de bordas. Para responder a esse questionamento, é necessário, em um primeiro momento, identificar as bases de dados mais utilizadas nos trabalhos selecionados e, em um segundo momento, relacionar os métodos e os valores que atingiram o estado da arte. 

A Tabela \ref{table:bases_dados} contém as bases de dados utilizadas em cada um dos artigos. Podemos observar, por essa tabela, que 14 dos 15 artigos selecionados utilizaram a base de dados BSDS500 \cite{BSDS500:2011} para a tarefa de detecção de bordas. Em segundo lugar no \textit{ranking} de utilização, com 10 artigos, está a base NYUDv2 \cite{NYUDv2:2012}. Outras bases citadas foram as bases SYNS \cite{SYNS:2015}, Multicue \cite{Multicue:2016} e Pascal VOC12 \cite{VOC12:2012}, sendo que a última foi utilizada nos artigos para treinamento da rede neural, uma vez que não é indicada à tarefa de detecção de bordas.

%\label{table:bases_dados}
\input{tables/bases_dados}

A Tabela \ref{table:bases_dados} contém, além das bases de dados, o ano e o mês de publicação dos artigos e o valor da métrica ODS (\textit{Optimal Data Set Scale}) para os \textit{datasets} BSDS500 e NYUDv2. Essa métrica corresponde ao melhor valor obtido para um conjunto de imagens \cite{Arbelaez:2009}. É importante destacar que os valores presentes nessa tabela utilizam os resultados produzidos pelas redes neurais incluindo as etapas de pós processamento existentes nos métodos. A partir da análise de resultados presentes na Tabela \ref{table:bases_dados}, é possível observar a evolução dos melhores resultados da literatura ao longo dos anos. 

Os valores da métrica ODS ainda procuram responder com mais precisão ao Critério de Qualidade \textit{C7} - \textit{``O artigo atingiu estado da arte em ao menos uma base de dados?''}. Conforme destacado na Seção \ref{sec:resultados}, diversos artigos informaram que haviam atingido o estado da arte. Pela análise de datas, é possível observar que alguns artigos que declaram atingir o estado da arte, não tiveram resultados suficientes para tal. Devido a temporalidade para publicações e o número de artigos publicados, é possível que um artigo tenham sido submetido sem que um resultado mais alto já houvesse sido publicado, levando a uma declaração inverídica do autor.

\let\thefootnote\relax\footnotetext{
  \\ \textsuperscript{1} BSDS500 - Berkeley Segmentation Data Set and Benchmarks 500 \cite{BSDS500:2011}
  \\ \textsuperscript{2} NYUDv2 - NYU Depth Dataset V2 \cite{NYUDv2:2012}
  \\ \textsuperscript{3} SYNS - The Southampton-York Natural Scenes dataset  \cite{SYNS:2015}
  \\ \textsuperscript{4} Pascal VOC12 - The PASCAL Visual Object Classes Challenge 2012 \cite{VOC12:2012}
  \\ \textsuperscript{5} Multicue - The multi-cue boundary detection dataset \cite{Multicue:2016}
}

A Tabela \ref{table:bases_dados} ainda exibe os valores corretos do estado da arte no fim de 2018, marcados em negrito. O estado da arte para a base de dados BSDS500, com valor 0.819, foi obtido pelos artigos \textit{``Richer convolutional features for edge detection''} \cite{RCF:2017:8100105} e \textit{``Cumulative nets for edge detection''} \cite{Cumulative:Song20181847}. Na base NYUDv2, o estado da arte, com valor 0.784, foi obtido pelo artigo \textit{``Convolutional oriented boundaries: From image segmentation to high-level tasks''} \cite{COB:2018:7917294}. 

Pode-se lembrar que a escolha do posicionamento das bordas é algo subjetivo entre pessoas e existe influência pessoal na definição do \textit{ground-truth} \cite{Marr:1980} \cite{MARTIN:1273918}, resultando em um valor médio de precisão humana. Para possibilitar um treinamento sem influência de fatores pessoais, são utilizados diferentes \textit{ground-truths} para uma mesma imagem. O cálculo de acerto ou erro de um método é dado em relação a maioria dos \textit{ground-truths}. 

A base de dados BSDS500 define que o limiar humano para seu conjunto de imagens é 0.803 \cite{amfm_pami2011}. Esse valor foi alcançado no ano de 2018, por 5 publicações. Esse resultado indica que, possivelmente, o aprendizado nessa base de dados está próximo a atingir seu potencial máximo. Com a evolução dos métodos, serão necessárias outras bases de dados para treinamento e avaliação. Espera-se, então, que a utilização da base de dados decresça nos próximos anos.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Hierarquia das bordas}
\label{ssec:hierarquia das bordas}

A Questão de Pesquisa \textit{QP3} busca identificar se os métodos analisados foram capazes de gerar hierarquias das bordas. Essa questão pode ser respondida com auxílio do Critério de Qualidade \textit{C8} - \textit{``O artigo foi capaz de gerar hierarquia das bordas?''}. Utilizando a Tabela \ref{table:artigos_selecionados}, podemos verificar que somente um artigo foi capaz de gerar hierarquia das bordas: \textit{``Convolutional oriented boundaries: From image segmentation to high-level tasks''} \cite{COB:2018:7917294}.

Nesse artigo é utilizado o conceito de hierarquia de regiões, ou \textit{Ultrametric Contour Maps} - Mapa de Contorno Ultramétrico \cite{COB:2018:7917294}, que estabele a relação entre os \textit{pixels} de bordas. A representação em mapas de contorno ultramétrico (UCM) permite que os valores sejam armazenados como valores de força das bordas, onde é possível limiarizar o resultado, com a seleção de um determinado nível de detalhe \cite{COB:2018:7917294}.

A classificação utilizada no COB para produzir a hierarquia de bordas (ou regiões) é baseada na ``força'' com que uma borda é reconhecida em relação a suas regiões adjacentes \cite{COB:2018:7917294}. Os resultados obtidos pela orientação das bordas e a ``força'' de reconhecimento, possibilita a criação de métodos de segmentação hierárquica \cite{COB:2018:7917294}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Conclusões} 
\label{sec:conclusoes}

A RSL aqui apresentada traçou um panorama do uso de redes neurais convolucionais para detecção de bordas e contornos nos últimos anos. Observa-se pelos resultados obtidos o aumento do uso dessa técnica para a solução do problema. Além disso, é possível observar, indiretamente, um crescimento do uso de redes neurais convolucionais como técnica utilizada para solução de diferentes problemas na área de computação.

Dentre as técnicas utilizadas nas redes neurais convolucionais, destacam-se as técnicas de \textit{transfer learning} e o uso de saídas laterais combinadas em um único resultado. A RSL indica que a rede neural VGG é a mais utilizada como base para criação de novas arquiteturas, seguida pela rede ResNet. A RSL indica ainda a existência de diferentes trabalhos que propõe técnicas para combinar os resultados laterais, produzindo um único classificador ou detector.

De acordo com a análise de artigos e seus resultados, observa-se o sucesso das redes convolucionais para a tarefa de detecção de bordas. Essa técnica alcançou o estado da arte nas bases de dados públicas BSDS500, NYUDv2, SYNS e Multicue. Salienta-se ainda, que o limiar humano foi alcançado na base BSDS500 no ano de 2018, por 5 trabalhos que utilizavam essa técnica.

Em relação a produção de hierarquia das bordas, observa-se a existência de um único trabalho que produziu esse tipo de resultado. Essa constatação indica existência de espaço na literatura para desenvolvimento de métodos que produzam esse tipo de resultado.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Referências}

\bibliographystyle{plain}
\bibliography{biblio,papers}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\end{document}




