import datetime
import os, shutil
from scipy import stats

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns

from sklearn import metrics
from sklearn.metrics import mean_squared_error
from sklearn.model_selection import train_test_split, cross_val_score, cross_val_predict

import re
import nltk
from nltk.corpus import stopwords
from nltk import word_tokenize
from nltk.tokenize import sent_tokenize, wordpunct_tokenize
from nltk.stem import PorterStemmer, WordNetLemmatizer, SnowballStemmer, LancasterStemmer

def load_data(filename):
    """Carrega os dados dos arquivos .csv, usados para treinamento e testes."""
    
    data = pd.read_csv(filename, delimiter=',')
    
    return data
    
def seaborn_plot(g, title=None, title_pos=1.03):
    """Plota gráficos para análise dos dados da base."""
    
    sns.set_theme(style="whitegrid")
    g.despine(left=True)
    
    if(g.legend != None):
        g.legend.set_title("")
        
    if(g.fig != None):
        g.fig.suptitle(title, y=title_pos, fontsize = 16)

def split_data(x_data, y_data, test_size=0.2):
  """Função para divisão dos conjuntos de treinamento e testes."""

  return train_test_split(
    x_data,
    y_data, 
    test_size = test_size, #percentual do conjunto de treino
    #random_state = 10 #seed random, para resultados semelhantes
  )
        
def preProcessCorpus(corpus):
    """Pre processa corpus."""
    
    newCorpus = [doc.lower() for doc in corpus]
    regex = r"(?<!\d)[.,;:-/'](?!\d)"
    newCorpus = [re.sub(regex, "", doc, 0) for doc in newCorpus]
    return newCorpus
        
def get_examples(data):
    """Retorna alguns exemplos do conjunto de treinamento, para inspeção visual."""
    
    return data[data.id.isin(['0efddbd69f2a05bd','72566f5506a6bc7c',
                              'f1db3bc888f45df3','ade4f00e120d6fe7',
                              '52f027f8f0bbf90c','c6a0bc8ef2f4128c',
                              'bcde39881cdea931','1c242ad34ec75d91',
                              'e44e2dcbc9f97273','d23713223cc6ec3e',
                              '3574ac0f83df6a13','76a0173e81e12b69',
                             ])]

def nltk_pre_processing(corpus):
    """Pré processa corpus, utilizando lemma, stem e expressões regulares."""
    
    wnl = WordNetLemmatizer()
    stem = SnowballStemmer('english')
    regex = r"(?<!\d)[.,;:-](?!\d)"
    
    corpus = re.sub(regex, "", corpus, 0)
    corpus = corpus.lower()
    corpus = wnl.lemmatize(corpus)
    corpus = stem.stem(corpus)
    return corpus
    

def do_lemma_stemmer(data, columns=['anchor','target']):
    """Pré processa dados em pandas, aplicando lematização, stemmer e remoção de stop words."""
    
    stop_words = set(stopwords.words('english'))
 
    for i in columns:
        data[i] = data[i].transform(
            lambda t: ' '.join([nltk_pre_processing(tok) for tok in nltk.word_tokenize(t) if tok.isalpha()])
            #lambda t: ' '.join([nltk_pre_processing(tok) for tok in wordpunct_tokenize(t)])
        )
        
        data[i] = data[i].transform(
            lambda t: ' '.join([tok for tok in nltk.word_tokenize(t) if tok not in stop_words])
        )
        
    return data

def calcula_correlacao(y_true, y_pred):
    """Estima correlação entre os valores preditos e previstos (evitando usar o Kaggle)."""
    
    pd_true = pd.DataFrame({'true': y_true})
    pd_pred = pd.DataFrame({'pred': y_pred})

    pd_pred['pred'] = pd_pred['pred'].transform(lambda x: 0. if x < 0. else (1. if x > 1. else x ))

    test_eval = pd.concat([pd_true, pd_pred], axis=1)
    
    return test_eval.corr()

def calcula_mse(y_true, y_pred):
    """Estima correlação entre os valores preditos e previstos, usando a métrica MSE."""
    
    return np.sqrt(mean_squared_error(y_true, y_pred))

def pre_process_data_codes(data, codes):
    """Adiciona informações de contexto aos dados (treinamento ou testes)."""
    
    data = data.merge(codes, left_on='context', right_on='code')
    data['title'] = data['title'].transform(lambda x: x.lower().replace(';', ''))
    #data['text'] = data['anchor'] + param.separador + data['target'] + param.separador + data['title']
    
    #data['anchor'] = data['anchor'] + '.' + data['title']
    #data['target'] = data['target'] + '.' + data['title']
    
    return data


def pre_process_codes(codes):
    """Adiciona informações de contexto (inclusive subcontextos) aos dados."""
    
    codes['code3'] = codes['code'].transform(lambda x: x[:3])
    codes['len3'] = codes['code'].transform(lambda x: len(x))
    codes['title'] = codes['title'].transform(lambda x: x.lower().replace(';', '.'))
    codes = codes[(codes.len3 > 1) & (codes.len3 <=4)]
    
    keys = pd.unique(codes.code3)
    dicionario = {}
    for k in keys:
        dicionario[k] = ''

    for code in codes.iterrows():
        key = code[1]['code3']
        title = code[1]['title']
        dicionario[key] += ' ' + title + '.'

    df = pd.DataFrame(list(dicionario.items()), columns = ['code', 'title'])
    df
    
    return df