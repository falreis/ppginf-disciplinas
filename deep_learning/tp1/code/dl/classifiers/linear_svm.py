import numpy as np
from random import shuffle
from past.builtins import xrange

def svm_loss_naive(W, X, y, reg):
    """
    Structured SVM loss function, naive implementation (with loops).

    Inputs have dimension D, there are C classes, and we operate on minibatches
    of N examples.

    Inputs:
    - W: A numpy array of shape (D, C) containing weights.
    - X: A numpy array of shape (N, D) containing a minibatch of data.
    - y: A numpy array of shape (N,) containing training labels; y[i] = c means
    that X[i] has label c, where 0 <= c < C.
    - reg: (float) regularization strength

    Returns a tuple of:
    - loss as single float
    - gradient with respect to weights W; an array of same shape as W
    """
    qtd_train = X.shape[0]
    qtd_classes = W.shape[1]
    
    dW = np.zeros(W.shape)
    loss = 0.0
    for i in xrange(qtd_train):
        scores = X[i].dot(W)
        correct_score = scores[y[i]]
        for j in xrange(qtd_classes):
            if j == y[i]:
                continue
            margin = scores[j] - correct_score + 1
            if margin > 0:
                loss += margin
                dW[:,j] += X[i] #para as 10 classes, adicionar todos os valores de X[i]
                dW[:, y[i]] -= X[i] #para y[i] (10 classes), remover X[i]

    #############################################################################
    # TODO:                                                                     #
    # Compute the gradient of the loss function and store it dW.                #
    # Rather that first computing the loss and then computing the derivative,   #
    # it may be simpler to compute the derivative at the same time that the     #
    # loss is being computed. As a result you may need to modify some of the    #
    # code above to compute the gradient.                                       #
    #############################################################################
    #média
    loss /= qtd_train
    dW /= qtd_train
    
    #regularização
    loss += reg * np.sum(W * W) # W*W causa overflow
    dW += reg * W #np.sum(W * W) # W*W causa overflow

    return loss, dW


def svm_loss_vectorized(W, X, y, reg):
    """
    Structured SVM loss function, vectorized implementation.

    Inputs and outputs are the same as svm_loss_naive.
    """
    #############################################################################
    # TODO:                                                                     #
    # Implement a vectorized version of the structured SVM loss, storing the    #
    # result in loss.                                                           #
    #############################################################################
    qtd_train = X.shape[0]
    qtd_classes = W.shape[1]
    
    dW = np.zeros(W.shape)
    loss = 0.0
    
    scores = X.dot(W)
    correct_score = scores[np.arange(scores.shape[0]), y]
    transp_score = np.matrix(correct_score).T
    margin = np.maximum(0, (scores - transp_score + 1))
    margin[np.arange(qtd_train),y] = 0 #zera itens onde j == y[i]
    
    # média e regularização
    loss = np.mean(np.sum(margin, axis=1))
    loss += reg * np.sum(W * W) # W*W causa overflow
    
    #############################################################################
    #                             END OF YOUR CODE                              #
    #############################################################################

    #############################################################################
    # TODO:                                                                     #
    # Implement a vectorized version of the gradient for the structured SVM     #
    # loss, storing the result in dW.                                           #
    #                                                                           #
    # Hint: Instead of computing the gradient from scratch, it may be easier    #
    # to reuse some of the intermediate values that you used to compute the     #
    # loss.                                                                     #
    #############################################################################
    margin[margin > 0] = 1 #somente positivos
    margin_sum = np.sum(margin, axis=1)
    margin[np.arange(qtd_train), y] = -np.matrix(margin_sum).T
    dW = np.dot(X.T, margin)
    
    #média e regularização
    dW /= qtd_train
    dW += reg * W #np.sum(W * W) # W*W causa overflow
    
    #############################################################################
    #                             END OF YOUR CODE                              #
    #############################################################################

    return loss, dW
    #return svm_loss_naive(W, X, y, reg)
